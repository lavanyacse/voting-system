<?php


  
 //============================================================+
// File name   : example_050.php
// Begin       : 2009-04-09
// Last Update : 2013-05-14
//
// Description : Example 050 for TCPDF class
//               2D Barcodes
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: 2D barcodes.
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');


//$sql = "SELECT * FROM users where secret=? ";
//$res=$db->query($sql);


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 050');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 050', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// NOTE: 2D barcode algorithms must be implemented on 2dbarcode.php class file.

// set font
$pdf->SetFont('helvetica', '', 11);

// add a page
$pdf->AddPage();

$txt = "DAPP VOTING SYSTEM\n";
$pdf->MultiCell(50, 100, $txt, 0, 'J', false, 1, 85, 10, true, 10, false, true, 0, 'T', false);


$tbl_header = '<table border="">';
$tbl_footer = '</table>';
$tbl ='';
$con=mysqli_connect("localhost","root","","newdb");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$result = mysqli_query($con,'SELECT * FROM users where secret="' . $_GET['secret'] .'"');
while($row = mysqli_fetch_array($result))
  {
  $Name = $row['username'];
  $Email = $row['email'];
  $dob = $row['dob'];
  $address = $row['address'];
$tbl = '<tr><td>'.$Name.'</td><td>'.$Email.'</td><td>' .$dob. '</td><td>' .$address. '</td></tr>';
}
// Print text using writeHTMLCell()
//$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '160');
$txt = "NAME:\n";
$pdf->MultiCell(18, 100, $txt, 0, 'J', false, 1, 17, 42, true, 10, true, true, 0, 'T', false);
$pdf->MultiCell(18, 80, $Name, 0, 'J', false, 1, 35, 42, true, 10, true, true, 0, 'T', false);

$txt = "EMAIL:\n";
$pdf->MultiCell(60, 100, $txt, 0, 'J', false, 1, 17, 50, true, 10, true, true, 0, 'T', false);
$pdf->MultiCell(60, 80, $Email, 0, 'J', false, 1, 35, 50, true, 10, true, true, 0, 'T', false);

$txt = "DOB:\n";
$pdf->MultiCell(22, 100, $txt, 0, 'J', false, 1, 20, 58, true, 10, true, true, 0, 'T', false);
$pdf->MultiCell(22, 80, $dob, 0, 'J', false, 1, 35, 58, true, 10, true, true, 0, 'T', false);

$txt = "ADDRESS:\n";
$pdf->MultiCell(50, 100, $txt, 0, 'J', false, 1, 10, 66, true, 10, true, true, 0, 'T', false);
$pdf->MultiCell(60, 80, $address, 0, 'J', false, 1, 35, 66, true, 10, true, true, 0, 'T', false);
$pdf->SetFont('helvetica', '', 10);



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// set style for barcode
/* $style = array(
	'border' => true,
	'vpadding' => 'auto',
	'hpadding' => 'auto',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255)
	'module_width' => 1, // width of a single module in points
	'module_height' => 1 // height of a single module in points
);
*/
// write RAW 2D Barcode

//$code = 'c4cbd8c84b73a781d3a036dc459f8364';
//$pdf->write2DBarcode($code, 'RAW', 80, 30, 30, 20, $style, 'N');

// write RAW2 2D Barcode
//$code = '[c4cbd8c84b73a781d3a036dc459f8364]';
//$pdf->write2DBarcode($code, 'RAW2', 80, 60, 30, 20, $style, 'N');
/*require_once("connect.php");
$sql = "SELECT * FROM users WHERE secret =5559ee066c807587dddc451592982fe1";
$res=$db->query($sql);
print_r($res);exit;*/

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// set style for barcode
$style = array(
	'border' => 2,
	'vpadding' => 'auto',
	'hpadding' => 'auto',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255)
	'module_width' => 1, // width of a single module in points
	'module_height' => 1 // height of a single module in points
);
//$txt = "Name\n";
//$pdf->MultiCell(50, 100, $txt, 0, 'J', false, 1, 85, 10, true, 10, false, true, 0, 'T', false);
//$pdf->writeHTML($_GET["username"]);
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($_GET["secret"], 'QRCODE,L', 130, 27, 60, 60, $style, 'N');
//$pdf->Text(140, 25, 'QRCODE L');

// QRCODE,M : QR-CODE Medium error correction
/*$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,M', 20, 90, 50, 50, $style, 'N');
$pdf->Text(20, 85, 'QRCODE M');

// QRCODE,Q : QR-CODE Better error correction
$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,Q', 20, 150, 50, 50, $style, 'N');
$pdf->Text(20, 145, 'QRCODE Q');

// QRCODE,H : QR-CODE Best error correction
$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,H', 20, 210, 50, 50, $style, 'N');
$pdf->Text(20, 205, 'QRCODE H');*/

// -------------------------------------------------------------------
// PDF417 (ISO/IEC 15438:2006)

/*

 The $type parameter can be simple 'PDF417' or 'PDF417' followed by a
 number of comma-separated options:

 'PDF417,a,e,t,s,f,o0,o1,o2,o3,o4,o5,o6'

 Possible options are:

 	a  = aspect ratio (width/height);
 	e  = error correction level (0-8);

 	Macro Control Block options:

 	t  = total number of macro segments;
 	s  = macro segment index (0-99998);
 	f  = file ID;
 	o0 = File Name (text);
 	o1 = Segment Count (numeric);
 	o2 = Time Stamp (numeric);
 	o3 = Sender (text);
 	o4 = Addressee (text);
 	o5 = File Size (numeric);
 	o6 = Checksum (numeric).

 Parameters t, s and f are required for a Macro Control Block, all other parametrs are optional.
 To use a comma character ',' on text options, replace it with the character 255: "\xff".

*/

//$pdf->write2DBarcode('www.tcpdf.org', 'PDF417', 80, 90, 0, 30, $style, 'N');
//$pdf->Text(80, 85, 'PDF417 (ISO/IEC 15438:2006)');

// -------------------------------------------------------------------
// DATAMATRIX (ISO/IEC 16022:2006)

//$pdf->write2DBarcode('http://www.tcpdf.org', 'DATAMATRIX', 80, 150, 50, 50, $style, 'N');
//$pdf->Text(80, 145, 'DATAMATRIX (ISO/IEC 16022:2006)');

// -------------------------------------------------------------------

// new style
/*$style = array(
	'border' => 2,
	'padding' => 'auto',
	'fgcolor' => array(0,0,255),
	'bgcolor' => array(255,255,64)
);

// QRCODE,H : QR-CODE Best error correction
$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,H', 80, 210, 50, 50, $style, 'N');
$pdf->Text(80, 205, 'QRCODE H - COLORED');

// new style
$style = array(
0	'border' => false,
	'padding' => 0,
	'fgcolor' => array(128,0,0),
	'bgcolor' => false
);

// QRCODE,H : QR-CODE Best error correction
$pdf->write2DBarcode('www.tcpdf.org', 'QRCODE,H', 140, 210, 50, 50, $style, 'N');
$pdf->Text(140, 205, 'QRCODE H - NO PADDING');*/

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_050.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>